Title: About

----

Columnleft:

- 
  columnleft:
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      _key: textLeft
      _uid: textLeft_1595007200798_182
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      _key: textLeft
      _uid: textLeft_1595007213904_691
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      _key: textLeft
      uniquekey: 0
      showpreviewinitially: false
      _uid: textLeft_1595019965581_574
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      _key: textLeft
      uniquekey: 1
      showpreviewinitially: false
      _uid: textLeft_1595019967176_680
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      _key: textLeft
      uniquekey: 2
      showpreviewinitially: false
      _uid: textLeft_1595019968469_786
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      _key: textLeft
      uniquekey: 3
      showpreviewinitially: false
      _uid: textLeft_1595019969807_892
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      _key: textLeft
      uniquekey: 4
      showpreviewinitially: false
      isnew: true
      _uid: textLeft_1595019971337_998
  _key: gridLeft
  _uid: gridLeft_1595007199812_122

----

Columnright:

- 
  columnright:
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      date: 2020-07-06
      _key: textRight
      _uid: textRight_1595007204433_337
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      date: 2020-07-06
      _key: textRight
      uniquekey: 0
      showpreviewinitially: false
      isnew: true
      _uid: textRight_1595025116886_1350
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      date: 2020-07-06
      _key: textRight
      uniquekey: 1
      showpreviewinitially: false
      isnew: true
      _uid: textRight_1595025142765_1557
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      date: 2020-07-06
      _key: textRight
      uniquekey: 2
      showpreviewinitially: false
      _uid: textRight_1595025162051_1740
    - 
      heading: Les Demoiselles
      text: 'Zu den bekanntesten Werken Picassos gehört das Gemälde Les Demoiselles d’Avignon (1907). Es avancierte zum Schlüsselbild der Klassischen Moderne.[1] Mit Ausnahme des monumentalen Gemäldes Guernica (1937), einer künstlerischen Umsetzung der Schrecken des Spanischen Bürgerkriegs, hat kein anderes Kunstwerk des 20. Jahrhunderts die Forschung so herausgefordert wie die Demoiselles.[1] Das Motiv der Taube auf dem Plakat, das er im Jahr 1949 für den Pariser Weltfriedenskongress entwarf, wurde weltweit zum Friedenssymbol.'
      date: 2020-07-06
      _key: textRight
      uniquekey: 3
      showpreviewinitially: false
      isnew: true
      _uid: textRight_1595025165300_1898
  _key: gridRight
  _uid: gridRight_1595007202394_277