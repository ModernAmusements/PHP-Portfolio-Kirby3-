<div class="grid">
<footer>
  <h3>
    <a href="<?= url() ?>"><?= $site->title() ?>
  </h3>
  <div class="text-right">
    <p><?= date('Y') ?> &copy; All Rights Reserved</p>
  </div>
  </a>
</footer>
</div>
<?= js('assets/js/libs.js') ?>
<?= js('assets/js/scripts.js') ?>
<?= js('assets/js/main.js') ?>
</body>
</html>