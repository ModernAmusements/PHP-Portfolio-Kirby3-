<?php snippet('header') ?>
<div class="bg"></div>
<div id="coords" data-html2canvas-ignore="true">(0,0)</div>
<svg id="svg">
</svg>
<img id="img" />
<div class="homeRow-1">

<div class="intro dragIndicator" id="draggable">
<p>Experimental online poster generator. Feel free to play arround.
Just hover over any element and move it around :)</p>
  </div>
  
  </div>
  

<div class="homeRow-2">
  <div class="designer dragIndicator" id="draggable">
    <h1 class="text-xl-ogg">Designer</h1>
    
  </div>

  <div class="developer dragIndicator" id="draggable">
    <h1 class="text-l-ogg-italic ">Developer</h1>
  </div>
</div>
<div class="homeRow-3">

  <div class="contact dragIndicator" id="draggable">
    <p>Shady Tawfik™<br>
      Digital & Type<br>
      German-Based Digital Design<br>
      tawfik@me.com<br>
      +31(0)639721282<br>
      instagram

          </p>    
  </div>
  <div class="passion dragIndicator" id="draggable">
    <h4>Function is nice.</h4>
    <p>But with all the resources out there, creating functinal products became somewhat feasible. Somewhere in the
process, we forgot beauty.
    </p>
  </div>
</div>
<div class="homeRow-4">
  <div class="save" id="draggable">
    <div class="footer" data-html2canvas-ignore="true">
      <a href="#" class="type-lg" id="save" target="_blank">
        Hit ("S") to Save! (as .png)
      </a>
    </div>
  </div>
  <div class="instructionDrag" id="draggable">
    <p>Drag and Drop </p>
  </div>
  <div class="instructionClick" id="draggable">
    <p>Click to create Spray Lines </p>
  </div>
</div>
<?php snippet('footerHome') ?>